#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define PWM_PIN 0
#define FREQ 25000

const char* ssid = "";
const char* password = "";
const char* mqtt_server = "192.168.178.";
const char* mqtt_user = "openhabian";
const char* mqtt_pwd = "";

char message_buff[3];

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
char msg[50];

int pwm = 0;
int pwm_old = 0;

void setup() {
  analogWriteFreq(FREQ);
  pinMode(PWM_PIN, OUTPUT);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  WiFi.begin(ssid, password);

  while (WiFi.status() !=   WL_CONNECTED) {
    delay(500);
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  int i;
  for (i = 0; i < length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';
  const char *p_payload = message_buff;
  pwm = atoi(p_payload);
}

void reconnect() {
  while (!client.connected()) {
    if (client.connect("ESP8266Client", mqtt_user, mqtt_pwd)) {
      client.subscribe("inTopic");
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  if (pwm_old != pwm) {
    analogWrite(PWM_PIN, pwm);
    pwm_old = pwm;
  }
}